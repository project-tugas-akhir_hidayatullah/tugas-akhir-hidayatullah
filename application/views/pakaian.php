<div class="container-fluid">
        <div id="carouselExampleSlidesOnly" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
            <img src="<?php echo base_url('uploads/slide1.png')?>" class="d-block w-100 mb-5" alt="...">
            </div>
        </div>
        </div>

    <div class="row text-center">

        <?php foreach ($pakaian as $brg) : ?>

        <div class="card ml-5 mb-5" style="width: 16rem;">
            <img src="<?php echo base_url().'/uploads/'.$brg->gambar ?>" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title mb-1"><?php echo $brg->nama_brg ?></h5>
                <small><?php echo $brg->keterangan?></small><br>
                <span class="badge badge-pill badge-success mb-3">Rp. <?php echo number_format ($brg->harga, 0,',','.') ?></span>
                <?php echo anchor('dashboard/tambah_ke_keranjang/'.$brg->id_brg,'<div class="btn btn-sm btn-primary">Tambah Ke Keranjang</div>')?>
                <?php echo anchor('dashboard/detail/'.$brg->id_brg,'<div class="btn btn-sm btn-success">Detail</div>')?>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
</div>