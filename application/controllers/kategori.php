<?php

class Kategori extends CI_Controller{
        public function Pakaian()
        {
            $data['pakaian'] =$this->model_kategori->data_pakaian()->result();
            $this->load->view('templates/header');
            $this->load->view('templates/sidebar');
            $this->load->view('pakaian',$data);
            $this->load->view('templates/footer');
        }

        public function Celana()
        {
            $data['Celana'] =$this->model_kategori->data_Celana()->result();
            $this->load->view('templates/header');
            $this->load->view('templates/sidebar');
            $this->load->view('Celana',$data);
            $this->load->view('templates/footer');
        }
        
        public function Sepatu()
        {
            $data['Sepatu'] =$this->model_kategori->data_Sepatu()->result();
            $this->load->view('templates/header');
            $this->load->view('templates/sidebar');
            $this->load->view('Sepatu',$data);
            $this->load->view('templates/footer');
        }

}